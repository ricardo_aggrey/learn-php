@extends('layouts.base')

@section('styles')
@endsection

@section('content')
<section class="h-screen w-screen p-8 bg-[FEFEFF]">
    <div class="h-32 w-full">
        <a href="{{ route('blog.create')}}" class="px-4 py-4 bg-blue-600 rounded text-white uppercase">Create</a>
    </div>
    <div class="grid grid-cols-3 gap-2">
        @foreach($blogs as $blog)
        <div class="bg-blue-200 flex-1 py-6 px-2 rounded shadow-sm">
            <div class="w-auto h-36 rounded space-y-2">
                <h1 class="text-2xl font-bold">ID: {{$blog->id}}</h1>
                <h2 class="text-xl opacity-90">Title: {{$blog->title}}</h2>
                <p class="text-base">Content: {{$blog->content}}</p>
            </div>
            <div class="h-8 w-full flex justify-between">
                <a href="{{ route('blog.edit', $blog->id)}}" class="text-white text-2xl">Edit</a>
                <form action="{{ route('blog.destroy', $blog->id)}}" method="post">
                    @csrf
                    @method('DELETE')
                    <button class="text-red-700 text-2xl" type="submit">Delete</button>
                </form>
            </div>
        </div>
        @endforeach
    </div>
</section>
@endsection
