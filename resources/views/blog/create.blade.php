@extends('layouts.base')

@section('content')
<section>
    <div class="container mx-auto">
        <div class="w-3/5 mx-auto">
            <h1 class="text-lg text-blue-600 uppercase py-4">Create Blog</h1>
            <form method="post" action="{{ route('blog.store') }}" class="flex flex-col space-y-4">
                @csrf
                <input type="number" class="py-3 px-2 border border-blue-500 rounded" placeholder="Blog #" name="id" />
                <input type="text" class="py-3 px-2 border border-blue-500 rounded" placeholder="title" name="title" />
                <input type="text" class="py-3 px-2 border border-blue-500 rounded" placeholder="blog content" name="content" />
                <button type="submit" class="px-2 py-4 bg-blue-600 rounded text-white uppercase">
                    Submit
                </button>
        </div>
        </form>
    </div>
</section>

@endsection
