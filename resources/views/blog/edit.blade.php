@extends('layouts.base')

@section('content')
<section>
    <div class="container mx-auto">
        <div class="w-3/5 mx-auto">
            <h1 class="text-lg text-blue-600 uppercase py-4">Edit Blog {{$blog->id}}</h1>
            <form method="post" action="{{ route('blog.update', $blog->id ) }}" class="flex flex-col space-y-4">
                @csrf
                @method('PATCH')
                <input type="number" class="py-3 px-2 border border-blue-500 rounded" placeholder="Blog #" name="id" disabled value="{{$blog->id}}" />
                <input type="text" class="py-3 px-2 border border-blue-500 rounded" placeholder="title" name="title" value="{{$blog->title}}" />
                <input type="text" class="py-3 px-2 border border-blue-500 rounded" placeholder="blog content" name="content" value="{{$blog->content}}" />
                <button type="submit" class="px-2 py-4 bg-blue-600 rounded text-white uppercase">
                    Submit
                </button>
        </div>
        </form>
    </div>
</section>

@endsection
