<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <title>learn php blog</title>
</head>

<body>
    <div class="h-screen w-screen">
        <h1 class="text-lg text-blue-600">Setup completed</h1>
    </div>
</body>

</html>
