<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    use HasFactory;

    protected $fillable = ['id', 'title', 'content'];
    public $blogs = array();


    public static function addBlog($blog)
    {
        array_push($blogs, $blog);
        print($blogs);
        return $blogs;
    }
}
