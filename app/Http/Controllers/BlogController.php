<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use Facade\FlareClient\Http\Response;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index()
    {
        $blogs = Blog::all();
        // dd($blogs);
        return view('blog.index', compact('blogs'));
    }

    public function create()
    {
        return view('blog.create');
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'id' => 'required|string',
            'title' => 'required|string|min:5|max:100',
            'content' => 'required|string|min:5|max:2000',
        ]);

        $blog = Blog::create($validated);
        return redirect('/blog');
    }

    public function show($id)
    {
    }


    public function edit($id)
    {
        $blog = Blog::findOrFail($id);

        return view('blog.edit', compact('blog'));
    }

    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'title' => 'required|string|min:5|max:100',
            'content' => 'required|string|min:5|max:2000',
        ]);

        Blog::whereId($id)->update($validated);

        return redirect('/blog');
    }

    public function destroy($id)
    {
        $blog = Blog::findOrFail($id);
        $blog->delete();

        return redirect('/blog');
    }
}
